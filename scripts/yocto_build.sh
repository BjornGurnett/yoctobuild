#!/bin/bash

#source the build enviroment
. ./oe-init-build-env build 
# move the local.conf file to the build directory
mv ${WORKSPACE}/poky/yoctobuild/build-conf/local.conf ${WORKSPACE}/poky/build/conf
# add layers
bitbake-layers add-layer ../meta-qt6
bitbake-layers add-layer ../meta-openembedded/meta-oe
bitbake-layers add-layer ../meta-openembedded/meta-python
bitbake-layers add-layer ../meta-openembedded/meta-networking
bitbake-layers add-layer ../meta-arm/meta-arm-toolchain
bitbake-layers add-layer ../meta-arm/meta-arm
bitbake-layers add-layer ../meta-ti/meta-ti-bsp
bitbake-layers add-layer ../meta-thermia
# bitbake the image
#bitbake -k thermia-image
bitbake -k meta-toolchain-qt6